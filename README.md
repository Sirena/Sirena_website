# Sirena_website
Данный репозиторий создан для [сайта](http://www.sirena.a0001.net) от [репозитория](https://codeberg.org/Sirena/SS14-Sirena)

# Куда жаловаться?
Жалобы и предложения можно оставить или на [discord](https://discord.gg/a87nCg3bHD) сервере, или по почте: sirena.server@mail.ru

# Состояние выгрузки файлов
[![status-badge](https://ci.codeberg.org/api/badges/12205/status.svg)](https://ci.codeberg.org/repos/12205)
